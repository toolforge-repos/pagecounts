# Wikimedia Portals pagecounts tool

This folder contains a script, pagecounts.php, that aggregates the article-count
statistics for the Wikimedia portal pages (e.g. www.wikipedia.org).

The PHP script is intended to run on WMF Toolforge and utilizes the database
replicas to query the article count for each wiki.

It runs once an hour and produces a JSON file in /public_html/pagecounts.json on
the server. That file is publicly available via this URL

https://pagecounts.toolforge.org/pagecounts.json

During the portals build step, that file is requested and merged with other
localization data to produce the stats on the portals pages.
