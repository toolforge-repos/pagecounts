#!/bin/bash

set -e

php pagecounts.php > ../../public_html/tmp.json
mv ../../public_html/tmp.json ../../public_html/pagecounts.json
